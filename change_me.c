/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  change_me.c      - main file

  include your name there and license for distribution.

  Remove next text: This line should not appear in submitted
  work and project name should be change to match real application.
  If this text is there I want 10 points subtracted from final
  evaluation.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"

void *spiled_base;
void *parlcd_base;

union knob {
  uint32_t d;
  struct{
    uint8_t b,g,r;
    bool bp : 1;
    bool gp : 1;
    bool rp : 1;
  };
};

union knob knobs(void){
  uint32_t *knobs = spiled_base + SPILED_REG_KNOBS_8BIT_o;
  return ((union knob){.d = *knobs});
}

union pixel {
  uint16_t d;
  struct{
    int b :5;
    int g :6;
    int r :5;
  };

};

union rgb{
  uint32_t d;
  struct{
    uint8_t b, g, r;
  };
};

union pixel fb[480][320];

void lcdframe(void){
  parlcd_write_cmd(parlcd_base, 0x2c);
  for (int i = 0; i < 320 ; i++) {
    for (int j = 0; j < 480 ; j++) {
      parlcd_write_data(parlcd_base, fb[j][i].d);
    }
  }
}

void rgb1(union rgb color){
  uint32_t *reg = spiled_base + SPILED_REG_LED_RGB1_o;
  *reg = color.d;
}
int main(int argc, char *argv[])
{

  /* Serialize execution of applications */

  /* Try to acquire lock the first */
  if (serialize_lock(1) <= 0) {
    printf("System is occupied\n");

    if (1) {
      printf("Waitting\n");
      /* Wait till application holding lock releases it or exits */
      serialize_lock(0);
    }
  }
  parlcd_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  assert(parlcd_base != NULL);
  parlcd_hx8357_init(parlcd_base);
  for (int i = 0; i < 320; i++){
    for (int j = 0; j < 480; j++){
      fb[j][i].d = 0xff;
    }
  }


  lcdframe();
  spiled_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  assert(spiled_base != NULL);
  rgb1((union rgb){.r = 0x1f});
  
  printf("Hello world\n");

  sleep(4);

  printf("Goodbye world\n");

  /* Release the lock */
  serialize_unlock();

  return 0;
}
