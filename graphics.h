#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "draw_utils.h"
#include "font_types.h"

#define WIN_TEXT 60
#define WIN_TEXT_Y 140
#define PRESS_TEXT 150
#define PRESS_TEXT_Y 300
#define TEXT_START 70
#define TEXT_START_Y 30
void view_scoreflash(int red_score, int blue_score, unsigned short *fb, font_descriptor_t *fdes);
void set_score_limit(int score_limit, unsigned short *fb, font_descriptor_t *fdes, unsigned char *parlcd_mem_base);
void black_the_display(unsigned short *fb, unsigned char *parlcd_mem_base);
void draw_winner_screen(bool winner,unsigned short *fb, font_descriptor_t *fdes);
void view_scoreflash(int red_score, int blue_score, unsigned short *fb, font_descriptor_t *fdes);
