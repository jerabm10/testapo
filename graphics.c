#define _POSIX_C_SOURCE 200112L
 
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
#include <stdbool.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "draw_utils.h"
#include "font_types.h"
#include "graphics.h"


void draw_winner_screen(bool winner,unsigned short *fb, font_descriptor_t *fdes){

    if (!winner){
        draw_word(WIN_TEXT, WIN_TEXT_Y, "BLUE WINS", 5, 0x7ff, fb, fdes);
        draw_word(PRESS_TEXT, PRESS_TEXT_Y, "Press knob to continue", 1, 0x7ff, fb, fdes);
    }else{
        draw_word(WIN_TEXT, WIN_TEXT_Y, "RED WINS", 5, 0x7ff, fb, fdes);
        draw_word(PRESS_TEXT, PRESS_TEXT_Y, "Press knob to continue", 1, 0x7ff, fb, fdes);
    }
}
void view_scoreflash(int red_score, int blue_score, unsigned short *fb, font_descriptor_t *fdes){
    if (red_score == 10){
        draw_char(195, 10, '1', 0x7ff, 3, fb, fdes);
        draw_char(210, 10, '0', 0x7ff, 3, fb, fdes);
        draw_char(235, 10, ':', 0x7ff, 3, fb, fdes);
        draw_char(250, 10, 48 + blue_score, 0x7ff, 3, fb, fdes);
    }else if (blue_score == 10){
        draw_char(210, 10, 48 + red_score, 0x7ff, 3, fb, fdes);
        draw_char(235, 10, ':', 0x7ff, 3, fb, fdes);
        draw_char(250, 10, '1', 0x7ff, 3, fb, fdes);
        draw_char(265, 10, '0', 0x7ff, 3, fb, fdes);
    }else{
    draw_char(210, 10, 48 + red_score, 0x7ff, 3, fb, fdes);
    draw_char(235, 10, ':', 0x7ff, 3, fb, fdes);
    draw_char(250, 10, 48 + blue_score, 0x7ff, 3, fb, fdes);
    }
}

// nejak ukoncit hru korektne


void set_score_limit(int score_limit, unsigned short *fb, font_descriptor_t *fdes, unsigned char *parlcd_mem_base){
    //set the score limit        
    draw_word(TEXT_START, TEXT_START_Y, "SET SCORE LIMIT", 3, 0x7ff, fb, fdes);
    if (score_limit == 10){
        draw_char(210, 150, '1', 0x7ff, 3, fb, fdes);
        draw_char(225, 150, '0', 0x7ff, 3, fb, fdes);
    }else{
        draw_char(220,150,48 + score_limit, 0x7ff, 3, fb, fdes);
    }

    for (int ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}
void black_the_display(unsigned short *fb, unsigned char *parlcd_mem_base){
    for (int ptr = 0; ptr < 320*480 ; ptr++) {    //black the display
        fb[ptr]=0u;
      }
      for (int ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
}


