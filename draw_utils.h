#include <stdint.h>
#include "font_types.h"

unsigned int hsv2rgb_lcd(int hue, int saturation, int value);
/*COLOR SETTING
hsv format*/
void draw_pixel(int x, int y, unsigned short color, unsigned short *fb);
void draw_pixel_big(int x, int y, unsigned short color, int scale, unsigned short *fb);
int char_width(int ch, font_descriptor_t *fdes);
void draw_char(int x, int y, char ch, unsigned short color, int scale, unsigned short *fb, font_descriptor_t *fdes);
void draw_word(int start_pos_x, int start_pos_y, char *string, int size, short color, unsigned short *fb, font_descriptor_t *fdes);