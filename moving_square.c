/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON
 
  include your name there and license for distribution.
  
 *******************************************************************/
 
#define _POSIX_C_SOURCE 200112L
 
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
#include <stdbool.h>
#include <math.h>

 
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "draw_utils.h"
#include "graphics.h"
unsigned short *fb;
font_descriptor_t *fdes;

 
int main(int argc, char *argv[]) {
  unsigned char *parlcd_mem_base, *mem_base;
  int i,j,score_limit;
  int ptr;
  unsigned int c;
  fb  = (unsigned short *)malloc(320*480*2);
 
  printf("PICO JEDEME\n");
 
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL)
    exit(1);
 
  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (mem_base == NULL)
    exit(1);
 
  parlcd_hx8357_init(parlcd_mem_base);
 
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  ptr=0;
  for (i = 0; i < 320 ; i++) {
    for (j = 0; j < 480 ; j++) {
      c = 0;
      fb[ptr]=c;
      parlcd_write_data(parlcd_mem_base, fb[ptr++]);
    }
  }
 
  struct timespec loop_delay;
  loop_delay.tv_sec = 0;
  loop_delay.tv_nsec = 150 * 1000 * 100;
  int xx_blue=0, yy_blue=0, xx_red = 0, yy_red=0;
  int score_red = 0, score_blue = 0;
  fdes = &font_winFreeSystem14x16; //font variable init

  //------------------set score limit default = 10-----------------------------------------------------//
  while (1)
  {
    for (ptr = 0; ptr < 320*480 ; ptr++) {    //black the display
        fb[ptr]=0u;
    }
    int r = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    score_limit = ((r>>8)&0xff)/4%10+1;
    set_score_limit(score_limit, fb, fdes,parlcd_mem_base);
    if ((r&0x7000000)!=0) {
      black_the_display(fb, parlcd_mem_base);
      sleep(1);
      break;
    }
  }

  //----------------------end set score limit-----------------------------------------------------//

  int r = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
  int old_tmp_blue = (r&0xff);
  int old_yy_blue = 130;
  int old_tmp_red = ((r>>16)&0xff);
  int old_yy_red = 130;
  int xx_ball = 230;
  int yy_ball = 150;
  bool right = true;
  bool up = true;



  *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0;
  while (1) {

    for (ptr = 0; ptr < 320*480 ; ptr++) {    //black the display
        fb[ptr]=0u;
    }
 
    int r = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    if ((r&0x7000000)!=0) {
      break;
    }

    //    blue knob starts here    //

    xx_blue = 465;
    int tmp_blue = (r&0xff);
    int difference_blue = old_tmp_blue - tmp_blue;
    old_tmp_blue = tmp_blue;

    if (abs(difference_blue) > 100){
      difference_blue = 0;
    }
      
    yy_blue = old_yy_blue + difference_blue*320/256;//((r&0xff)*320)/256 - ((rr&0xff)*320)/256 ;
    
    if (yy_blue >= 260){
        yy_blue = 260;
    }
    if (yy_blue <= 0){
        yy_blue = 0;
    }
    old_yy_blue = yy_blue;
 
    for (j=0; j<60; j++) {
      for (i=0; i<15; i++) {
        draw_pixel(i+xx_blue,j+yy_blue,0x7ff, fb);
      }
    }

    //    blue knob ends here    //


    //    red knob starts here    //

    xx_red = 0;
    int tmp_red = ((r>>16)&0xff);
    int difference_red = old_tmp_red - tmp_red;
    old_tmp_red = tmp_red;

    if (abs(difference_red) > 100){
      difference_red = 0;
    }

    yy_red = old_yy_red + difference_red*320/256;//((r&0xff)*320)/256 - ((rr&0xff)*320)/256 ;
    
    
    //printf("%d\n",yy_red);
    if (yy_red >= 260){
        yy_red = 260;
    }
    if (yy_red <= 0){
        yy_red = 0;
    }
    old_yy_red = yy_red;

    for (j=0; j<60; j++) {
      for (i=0; i<15; i++) {
        draw_pixel(i+xx_red,j+yy_red,0x7ff, fb);
      }
    }

    //    red knob ends here    //

    //    ball starts here    //
    for (j=0; j<20; j++) {
      for (i=0; i<20; i++) {
        if (i+j < 6 || i+j >32 || j-i > 12 || i-j > 12){
          continue;
        }
        draw_pixel(i+xx_ball,j+yy_ball,0x7ff, fb);
      }
    }

    if (xx_ball == 445){
      if(yy_ball - yy_blue <= 55 && yy_ball - yy_blue >= -15){
        right = false;
      }else if(yy_ball - yy_blue > 55 && yy_ball - yy_blue <= 65){
        right = false;
        up = false;
      }else if((yy_ball - yy_blue < -15 && yy_ball - yy_blue >= -25)){
        right = false;
        up = true;
      }
    }
    if (xx_ball == 15){
      if(yy_ball - yy_red <= 55 && yy_ball - yy_red >= -15){
        right = false;
      }else if(yy_ball - yy_red > 55 && yy_ball - yy_red <= 65){
        right = false;
        up = false;
      }else if((yy_ball - yy_red < -15 && yy_ball - yy_red >= -25)){
        right = false;
        up = true;
      }
    }
    if (yy_ball < 5){
      up = false;
    }
    if (yy_ball > 295){      
      up = true;
    }

    if(up){
      yy_ball-=5;
    }else{
      yy_ball+=5;
    }

    if(right){
      xx_ball+=5;
    }else{
      xx_ball-=5;
    }
    // GAME END
    if (xx_ball < -20 || xx_ball > 480){
      if (xx_ball < -20){
        score_blue++;
      }else{
        score_red++;
      }
    
    printf("Score: %d : %d\n", score_red,score_blue);
    if (score_blue == score_limit){
        printf("BLUE WINS!!! %d : %d\n", score_red,score_blue);
        draw_winner_screen(0, fb, fdes);
        parlcd_write_cmd(parlcd_mem_base, 0x2c);      //draw into display
        for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
    }else if (score_red == score_limit){
        printf("RED WINS!!! %d : %d\n", score_red,score_blue);
        draw_winner_screen(1, fb, fdes);
        parlcd_write_cmd(parlcd_mem_base, 0x2c);      //draw into display
        for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
    }

    view_scoreflash(score_red, score_blue, fb, fdes);
    
    parlcd_write_cmd(parlcd_mem_base, 0x2c);      //draw into display
    for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }

      uint32_t val_line = (uint32_t)(pow(2,score_red)-1);
      val_line <<= (32-score_red);
      *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line + (uint32_t)(pow(2,score_blue)-1);

        
      srand(time(0));
      sleep(1);
      xx_ball = 230;
      yy_ball = rand() % 301;
      right = !right;
      up = !up;
      }
      //    ball ends here    //
    

    view_scoreflash(score_red, score_blue, fb, fdes);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);      //draw into display
    for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }


    
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
 /* ASI TO TU NEMUSI BEJT
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (ptr = 0; ptr < 480*320 ; ptr++) {
    parlcd_write_data(parlcd_mem_base, 0);
  }
 */
  printf("NAZDAR MORE\n");
  free(fb);
  return 0;
}